
package serviceweb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for lotDTO complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="lotDTO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *         &lt;element name="product" type="{http://serviceWeb/}productDTO" minOccurs="0"/&gt;
 *         &lt;element name="stock" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="warehouse" type="{http://serviceWeb/}warehouseDTO" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "lotDTO", propOrder = { "id", "product", "stock", "warehouse" })
public class LotDTO {

    protected Long id;
    protected ProductDTO product;
    protected Integer stock;
    protected WarehouseDTO warehouse;

    /**
     * Gets the value of the id property.
     *
     * @return
     *     possible object is
     *     {@link Long }
     *
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     *
     * @param value
     *     allowed object is
     *     {@link Long }
     *
     */
    public void setId(Long value) {
        this.id = value;
    }

    /**
     * Gets the value of the product property.
     *
     * @return
     *     possible object is
     *     {@link ProductDTO }
     *
     */
    public ProductDTO getProduct() {
        return product;
    }

    /**
     * Sets the value of the product property.
     *
     * @param value
     *     allowed object is
     *     {@link ProductDTO }
     *
     */
    public void setProduct(ProductDTO value) {
        this.product = value;
    }

    /**
     * Gets the value of the stock property.
     *
     * @return
     *     possible object is
     *     {@link Integer }
     *
     */
    public Integer getStock() {
        return stock;
    }

    /**
     * Sets the value of the stock property.
     *
     * @param value
     *     allowed object is
     *     {@link Integer }
     *
     */
    public void setStock(Integer value) {
        this.stock = value;
    }

    /**
     * Gets the value of the warehouse property.
     *
     * @return
     *     possible object is
     *     {@link WarehouseDTO }
     *
     */
    public WarehouseDTO getWarehouse() {
        return warehouse;
    }

    /**
     * Sets the value of the warehouse property.
     *
     * @param value
     *     allowed object is
     *     {@link WarehouseDTO }
     *
     */
    public void setWarehouse(WarehouseDTO value) {
        this.warehouse = value;
    }

}
