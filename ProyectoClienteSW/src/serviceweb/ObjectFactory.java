
package serviceweb;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the serviceweb package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 *
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Exception_QNAME = new QName("http://serviceWeb/", "Exception");
    private final static QName _FindLotsWarehouse_QNAME = new QName("http://serviceWeb/", "findLotsWarehouse");
    private final static QName _FindLotsWarehouseResponse_QNAME =
        new QName("http://serviceWeb/", "findLotsWarehouseResponse");
    private final static QName _FindWarehouses_QNAME = new QName("http://serviceWeb/", "findWarehouses");
    private final static QName _FindWarehousesResponse_QNAME =
        new QName("http://serviceWeb/", "findWarehousesResponse");
    private final static QName _GetAllWarehouse_QNAME = new QName("http://serviceWeb/", "getAllWarehouse");
    private final static QName _GetAllWarehouseResponse_QNAME =
        new QName("http://serviceWeb/", "getAllWarehouseResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: serviceweb
     *
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Exception }
     *
     */
    public Exception createException() {
        return new Exception();
    }

    /**
     * Create an instance of {@link FindLotsWarehouse }
     *
     */
    public FindLotsWarehouse createFindLotsWarehouse() {
        return new FindLotsWarehouse();
    }

    /**
     * Create an instance of {@link FindLotsWarehouseResponse }
     *
     */
    public FindLotsWarehouseResponse createFindLotsWarehouseResponse() {
        return new FindLotsWarehouseResponse();
    }

    /**
     * Create an instance of {@link FindWarehouses }
     *
     */
    public FindWarehouses createFindWarehouses() {
        return new FindWarehouses();
    }

    /**
     * Create an instance of {@link FindWarehousesResponse }
     *
     */
    public FindWarehousesResponse createFindWarehousesResponse() {
        return new FindWarehousesResponse();
    }

    /**
     * Create an instance of {@link GetAllWarehouse }
     *
     */
    public GetAllWarehouse createGetAllWarehouse() {
        return new GetAllWarehouse();
    }

    /**
     * Create an instance of {@link GetAllWarehouseResponse }
     *
     */
    public GetAllWarehouseResponse createGetAllWarehouseResponse() {
        return new GetAllWarehouseResponse();
    }

    /**
     * Create an instance of {@link LotDTO }
     *
     */
    public LotDTO createLotDTO() {
        return new LotDTO();
    }

    /**
     * Create an instance of {@link ProductDTO }
     *
     */
    public ProductDTO createProductDTO() {
        return new ProductDTO();
    }

    /**
     * Create an instance of {@link WarehouseDTO }
     *
     */
    public WarehouseDTO createWarehouseDTO() {
        return new WarehouseDTO();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}
     */
    @XmlElementDecl(namespace = "http://serviceWeb/", name = "Exception")
    public JAXBElement<Exception> createException(Exception value) {
        return new JAXBElement<Exception>(_Exception_QNAME, Exception.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindLotsWarehouse }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindLotsWarehouse }{@code >}
     */
    @XmlElementDecl(namespace = "http://serviceWeb/", name = "findLotsWarehouse")
    public JAXBElement<FindLotsWarehouse> createFindLotsWarehouse(FindLotsWarehouse value) {
        return new JAXBElement<FindLotsWarehouse>(_FindLotsWarehouse_QNAME, FindLotsWarehouse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindLotsWarehouseResponse }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindLotsWarehouseResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://serviceWeb/", name = "findLotsWarehouseResponse")
    public JAXBElement<FindLotsWarehouseResponse> createFindLotsWarehouseResponse(FindLotsWarehouseResponse value) {
        return new JAXBElement<FindLotsWarehouseResponse>(_FindLotsWarehouseResponse_QNAME,
                                                          FindLotsWarehouseResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindWarehouses }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindWarehouses }{@code >}
     */
    @XmlElementDecl(namespace = "http://serviceWeb/", name = "findWarehouses")
    public JAXBElement<FindWarehouses> createFindWarehouses(FindWarehouses value) {
        return new JAXBElement<FindWarehouses>(_FindWarehouses_QNAME, FindWarehouses.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindWarehousesResponse }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindWarehousesResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://serviceWeb/", name = "findWarehousesResponse")
    public JAXBElement<FindWarehousesResponse> createFindWarehousesResponse(FindWarehousesResponse value) {
        return new JAXBElement<FindWarehousesResponse>(_FindWarehousesResponse_QNAME, FindWarehousesResponse.class,
                                                       null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllWarehouse }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetAllWarehouse }{@code >}
     */
    @XmlElementDecl(namespace = "http://serviceWeb/", name = "getAllWarehouse")
    public JAXBElement<GetAllWarehouse> createGetAllWarehouse(GetAllWarehouse value) {
        return new JAXBElement<GetAllWarehouse>(_GetAllWarehouse_QNAME, GetAllWarehouse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllWarehouseResponse }{@code >}
     *
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetAllWarehouseResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://serviceWeb/", name = "getAllWarehouseResponse")
    public JAXBElement<GetAllWarehouseResponse> createGetAllWarehouseResponse(GetAllWarehouseResponse value) {
        return new JAXBElement<GetAllWarehouseResponse>(_GetAllWarehouseResponse_QNAME, GetAllWarehouseResponse.class,
                                                        null, value);
    }

}
