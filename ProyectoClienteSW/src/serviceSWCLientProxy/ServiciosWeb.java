package serviceSWCLientProxy;

import serviceweb.LotSW;
import serviceweb.LotServiceSW;
import serviceweb.ProductSW;
import serviceweb.ProductServerServiceSW;
import serviceweb.WarehouseSW;
import serviceweb.WarehouseServiceSW;

public class ServiciosWeb {
    
    
    private ProductServerServiceSW productServerServiceSW;
    private ProductSW productSW;
    
    private WarehouseServiceSW warehouseServerServiceSw;
    private WarehouseSW warehouseSW;
    
    private LotServiceSW lotServiceServiceSW;
    private LotSW lotSW; 
    
    
    public ServiciosWeb() {
        
        //
        productServerServiceSW = new ProductServerServiceSW();
        productSW = productServerServiceSW.getProductServerSWPort();
        
        warehouseServerServiceSw = new WarehouseServiceSW();
        warehouseSW=warehouseServerServiceSw.getWarehouseSWPort();
            
        lotServiceServiceSW= new LotServiceSW();
        lotSW =lotServiceServiceSW.getLotSWPort();
        
        
    }


    public void setProductSW(ProductSW productSW) {
        this.productSW = productSW;
    }

    public ProductSW getProductSW() {
        return productSW;
    }

    public void setWarehouseSW(WarehouseSW warehouseSW) {
        this.warehouseSW = warehouseSW;
    }

    public WarehouseSW getWarehouseSW() {
        return warehouseSW;
    }

    public void setLotSW(LotSW lotSW) {
        this.lotSW = lotSW;
    }

    public LotSW getLotSW() {
        return lotSW;
    }

}
